import React from 'react';
import Chat from '../../components/ChatParts/Chat/Chat';
import InputComponent from '../../components/input/Input.js';
import axios from 'axios';
import Header from '../../components/header/Header.js'



export const APIContext = React.createContext();

//Declaracao de API
let api = 
axios.create(
  {
      baseURL: 'https://treinamentoajax.herokuapp.com/'
  }
);

function App() {

    return (
        <section className="message-section">
          <APIContext.Provider value={api}>
            <Header/>
            <InputComponent />
            <Chat />
          </APIContext.Provider>
        </section>
    );
}

export default App;


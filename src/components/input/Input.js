import React, {useContext, useState} from 'react';
import './Input.css';
import {APIContext} from '../../pages/app/App';

function InputComponent(props) {
    const api = useContext(APIContext);

    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');

    const clickButton = (e) => {

        if (title === '' || message === '') {
            alert("Voce precisa preencher os dois campos mano");
            return;
        }else{
            api.post('messages', 
                {
                    "message": {
                        "name": title,
                        "message": message
                    }
                }
            )
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    return (
        <section className='input-component'>
                <h1>Chat IN Junior</h1>
            <h4>Nome:</h4>
            <input type="text" onChange={(e) => setTitle(e.target.value)}></input>
            <h4>Mensagem:</h4>
            <textarea type="text" onChange={(e) => setMessage(e.target.value)}></textarea>
            <button className="alinha" onClick={clickButton}>Enviar</button>
        </section>
    );
}

export default InputComponent;

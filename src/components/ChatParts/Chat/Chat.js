import React, {useState, useEffect, useContext} from 'react';
import Message from '../Message/Message';
import {APIContext} from '../../../pages/app/App'
import './Chat.css';

function Chat(props) {

    const api = useContext(APIContext);
    const [messages, setMessages] = useState([]);

    
    const updateMessages = () => {
        const fetchData = async () => {
            const response = await api.get('messages');
            const responseArray = response.data;
            setMessages(responseArray);
        }
        fetchData();
    }

    useEffect(updateMessages, [api, messages]);

    const onChildUpdateEvent = () => {
        setMessages([]);
    }
    
    let messageBoxes = [];
    for (let i in messages) {
        messageBoxes.push(
        <Message
            handleChange={onChildUpdateEvent}
            key={i}
            id={messages[i]['id']}
            title={messages[i]['name']}
            content={messages[i]['message']}
        />);
    }

    return (
        <section className="chat-box">
            {messageBoxes}
        </section>
    )
}

export default Chat;
